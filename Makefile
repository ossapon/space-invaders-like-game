#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/06 19:43:57 by soleksiu          #+#    #+#              #
#    Updated: 2018/10/06 19:43:58 by soleksiu         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

SRCDIR = src

OBJDIR = obj

SRC = main.cpp Asteroid.cpp AWeapon.cpp Boss.cpp Bullet.cpp Enemy.cpp Game.cpp \
	  GameTime.cpp Pistol.cpp Player.cpp SpaceObject.cpp Stars.cpp Wall.cpp \


OBJ = $(addprefix $(OBJDIR)/,$(SRC:.cpp=.o))

CXX = @clang++

CXXFLAGS = -std=c++98 -Wall -Wextra -Werror

NAME = ft_retro

all: objdir $(NAME)

$(NAME): $(OBJ)
	@$(CXX) $(CXXFLAGS) $(OBJ) -lncurses -o $(NAME) 

objdir: 
	@mkdir -p $(OBJDIR)

$(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	@$(CXX) $(CXXFLAGS) -I includes -c $< -o $@

backup: 
	@mkdir -p ~/Desktop/backup_rush00
	@cp -r * ~/Desktop/backup_rush00

clean:
	@rm -rf $(OBJ)

fclean: clean
	@rm -rf $(NAME)
	@find . -name "*~" -delete -or \
			-name "#*#" -delete -or \
			-name ".DS_Store" -delete -or \
			-name "obj" -delete -or \
			-name "a.out" -delete

re: fclean all
