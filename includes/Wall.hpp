// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Wall.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:53:37 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:53:53 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef WALL_HPP
# define WALL_HPP

#include "retro.hpp"

class Wall : public SpaceObject
{
public:
	Wall();
	Wall(Wall const &obj);
	Wall &operator=(Wall const &rhs);
	~Wall();
	
};

#endif
