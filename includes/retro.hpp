// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   retro.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 20:54:00 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 20:54:54 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef RETRO_HPP
# define RETRO_HPP
#include <ncurses.h>
#include <iostream>

# define BLACK   0
# define RED     1
# define GREEN   2
# define YELLOW  3
# define BLUE    4
# define MAGENTA 5
# define CYAN    6
# define WHITE   7
# define BOLD    8
# define BLINK   9
# define KEY_ESC 27
# define KEY_W   119
# define KEY_A   97
# define KEY_S   115
# define KEY_D   100
# define KEY_M   109
# define KEY_ENT 10
# define KEY_SP  32
# define KEY_P   112

typedef struct	s_coords
{
	int 		x;
	int			y;
}				t_coord;

#include "../includes/SpaceObject.hpp"
#include "../includes/Bullet.hpp"
#include "../includes/AWeapon.hpp"
#include "../includes/Player.hpp"
#include "../includes/Asteroid.hpp"
#include "../includes/Boss.hpp"
#include "../includes/Game.hpp"
#include "../includes/GameTime.hpp"
#include "../includes/Pistol.hpp"
#include "../includes/Stars.hpp"
#include "../includes/Wall.hpp"
#include "../includes/Enemy.hpp"

#endif
