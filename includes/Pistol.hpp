
#ifndef CPPPOOL_PISTOL_H
#define CPPPOOL_PISTOL_H

#include "retro.hpp"

class Pistol: public AWeapon {

public:
	//constructors
	Pistol();
	Pistol(Pistol const &);
	~Pistol();
	Pistol &operator=(Pistol const &);

	int 	attack() const;



};


#endif
