// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Game.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:49:05 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:49:23 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef GAME_HPP
# define GAME_HPP

#include "retro.hpp"

class Game
{
public:
	Game();
	Game(Game const &obj);
	Game &operator=(Game const &rhs);
	~Game();
	static void modyfiedText(int x, int y, const char *text, int color, int bold, int blink);
	void displayMenu(void);
	void displayStars(void);
	void displayEnemys(Player &player);
	int  keysHandler(Player &player);
	void displayShooting(void);
	int displayGameOver(Player &player);
	int displayPause(Player &player);
	void displayBoss(Player &player);
	WINDOW			*_wnd_ptr;
	static int		_scr_width;
	static int		_scr_height;
	bool			_show_menu;
	bool			_pause;
	bool			_bullet;
	static bool 	_collision;
};

#endif
