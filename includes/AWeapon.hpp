// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Weapon.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:54:02 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:54:23 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef WEAPON_HPP
# define WEAPON_HPP
#include "retro.hpp"

class AWeapon {

private:
	std::string		_name;
	int 			_apCost;
	int 			_damage;

public:

	//constructors
	AWeapon(std::string name, int apcost, int damage);
	AWeapon(AWeapon const &obj);
	~AWeapon();

	AWeapon 			&operator=(AWeapon const &rhs);

	//geter/seter
	std::string		getName() const;
	int 			getAPCost() const;
	int 			getDamage() const;

	//method
	virtual int	attack() const = 0;
	
};

#endif
