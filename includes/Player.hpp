// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Player.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:46:54 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:48:23 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PLAYER_HPP
# define PLAYER_HPP

#include "retro.hpp"

class Player: public SpaceObject {

private:
	int				_ap;
	AWeapon			*_weapon;
	long int		amountOfKilledEnemies;

public:
	Bullet			*bullet;
	static bool		_blink;

	//constructor
	Player();
	Player(Player const &obj);
	~Player();

	Player 			&operator=(Player const &rhs);

	// get-er/set-er
	std::string		getWeaponType() const ;
	int				getAP() const;
	long int		getAmount();
	void			setAmount(int i);

	//methods
	void			recover(SpaceObject *);
	void 			equip(AWeapon *);
	void 			attack();
	void			displayPlayer(void);

};

#endif
