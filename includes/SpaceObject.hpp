// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   SpaceObject.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:52:49 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:53:25 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SPACEOBJECT_HPP
# define SPACEOBJECT_HPP

#include "retro.hpp"

class SpaceObject
{
private:
	std::string		_type;
	int				_lvl;	

protected:
	int 			_val;
	int 			_speed;
	bool 			_collision;

public:
	int				_life;
	t_coord			_xy;
	//constructor
	SpaceObject(std::string type, int x, int y, int val, int speed);
	SpaceObject(SpaceObject const &obj);
	~SpaceObject();

	SpaceObject 	&operator=(SpaceObject const &rhs);

	// get-er/set-er
	std::string		getType() const;
	int				getVal() const;
	int 			getSpeed() const;
	int				getX() const;
	int				getY() const;
	void			setX(int x);
	void			setY(int y);
};

#endif
