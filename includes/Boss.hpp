// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Boss.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:50:50 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:51:08 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef BOSS_HPP
# define BOSS_HPP
#include "retro.hpp"

class Boss
{
protected:
	int				_hp;
	std::string		_type;
	double			_x;
	double			_y;
//	int				displayBullets;

public:
	Bullet			*bullet;
	//constructors
	Boss(void);
	Boss(std::string type, int hp);
	Boss(Boss const &obj);
	~Boss();

	Boss			&operator=(Boss const &rhs);

	//geter/seter
	std::string		getType() const;
	int 			getHP() const;
	double				getX() const;
	double				getY() const;
	//methods
	virtual void	takeDamage(int);
	void drawBoss(void);
	void moveBoss(Player &player);
	void displayBullet();
};

#endif
