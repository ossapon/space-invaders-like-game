// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Enemy.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:51:19 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:51:42 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ENEMY_HPP
# define ENEMY_HPP

#include "Enemy.hpp"
#include "Player.hpp"

class Enemy {

protected:
	int				_hp;
	std::string		_type;
	double			_x;
	double			_y;
	int				collision;
public:
	Bullet			*bullet;
	//constructors
	Enemy(void);
	Enemy(std::string type, int hp);
	Enemy(Enemy const &obj);
	~Enemy();

	Enemy			&operator=(Enemy const &rhs);

	//geter/seter
	std::string		getType() const;
	int 			getHP() const;

	//methods
	virtual void	takeDamage(int);
	void displayEnemy(void);
	void displayBullet(void);
	void moveEnemy(Player &pl1);

};

#endif
