// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Stars.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:53:37 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:53:53 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef Stars_HPP
# define Stars_HPP

#include "retro.hpp"

class Stars
{
public:
	Stars();
	Stars(Stars const &obj);
	Stars &operator=(Stars const &rhs);
	~Stars();
	void updateCoords(void);
	void displayStar(void);
	int _x;
	int _y;
};

#endif
