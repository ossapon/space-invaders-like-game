// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Asteroid.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:49:53 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:50:27 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ASTEROID_HPP
# define ASTEROID_HPP

#include "retro.hpp"

class Asteroid : public SpaceObject
{
public:
	Asteroid();
	Asteroid(Asteroid const &obj);
	Asteroid &operator=(Asteroid const &rhs);
	~Asteroid();
};

#endif
