
#ifndef BULLET_HPP
#define BULLET_HPP


#include "retro.hpp"

class Bullet: public SpaceObject {

public:
	Bullet(int x, int y);
	~Bullet();
	Bullet(Bullet const &obj);
	Bullet &operator=(Bullet const &rhs);

	void	displayBullet();
	void	move();
	int		checkCollision(int x, int y);

};

#endif
