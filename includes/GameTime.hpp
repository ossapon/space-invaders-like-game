// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   GameTime.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:52:09 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:52:31 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef GAMETIME_HPP
# define GAMETIME_HPP

#include "retro.hpp"

class GameTime
{
public:
	GameTime();
	GameTime(GameTime const &obj);
	GameTime &operator=(GameTime const &rhs);
	~GameTime();
	
};

#endif
