// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Game.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:40:48 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:41:01 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "../includes/retro.hpp"
#include <unistd.h>
#include <cstdlib>

Game::Game() : _show_menu(false), _pause(false) 
{
	_wnd_ptr = initscr();
	start_color();
	noecho(); 
	keypad(stdscr, TRUE); 
	curs_set(FALSE); 
	getmaxyx(stdscr, _scr_height, _scr_width); 
	nodelay(_wnd_ptr, true);
}

Game::Game(Game const &obj)
{
	(void)obj;

}

Game &Game::operator=(Game const &rhs)
{
	(void)rhs;
	return (*this);
}

Game::~Game()
{
	endwin();
}

void Game::modyfiedText(int x, int y, const char *text, int color, int bold, int blink)
{
	if (blink)
		attron(A_BLINK);// Задається aтрибут "мигання"
	if (color && color != COLOR_WHITE)
	{
		init_pair(1, color, COLOR_BLACK);
		attron(COLOR_PAIR(1)); // Застосувати колір
	}
	if (bold)
		attron(A_BOLD);	// Задається атрибут виводу "напівжирний" текст

	mvprintw(x, y, text); // запис у буфер по координатач y і x
	if (blink)
		attroff(A_BLINK);
	if (color && color != COLOR_WHITE)
		attroff(COLOR_PAIR(1)); // Відмінити атрибут
	if (bold)
		attroff(A_BOLD);
}

int Game::keysHandler(Player &player)
{
	int		key;

	key = getch();
	if (key == KEY_ESC)
		return (KEY_ESC);
	else if (key == KEY_M)
		this->_show_menu = this->_show_menu== false ? true :false;
	else if (key == KEY_P)
		this->_pause = this->_pause== false ? true :false;
	else if (key == KEY_W)
	{
		player.setY(player.getY() - 1);
		if (player.getY() - 8 < 0)
			player.setY(8);
	}
	else if (key == KEY_S)
	{
		player.setY(player.getY() + 1);
		if (player.getY() > Game::_scr_height - 8)
			player.setY(Game::_scr_height - 8);
	}
	else if (key == KEY_A)
	{
		player.setX(player.getX() - 2);
		if (player.getX() < 0)
			player.setX(0);
	}

	else if (key == KEY_D)
	{
		player.setX(player.getX() + 2);
		if (player.getX() > Game::_scr_width - 5)
			player.setX(Game::_scr_width - 5);
	}
	else if (key == KEY_SP)
	{
		this->_bullet  = true;
//		player.bullet = new Bullet(player.getX() + 2, player.getY() + 3);
		player.bullet->setY(player.getY() + 3);
		player.bullet->setX(player.getX() + 2);
		player.setAmount(100);
	}
	else if (key == KEY_P)
		this->_pause  = true;
	return (key);
}

void Game::displayMenu(void)
{
	modyfiedText(0, 0, "        Controls\n", 0, BOLD, 0);
	modyfiedText(1, 0, "Move top  . . . . . . W\n", 0, BOLD, 0);
	modyfiedText(2, 0, "Move left . . . . . . A\n", 0, BOLD, 0);
	modyfiedText(3, 0, "Move bottom . . . . . S\n", 0, BOLD, 0);
	modyfiedText(4, 0, "Move right  . . . . . D\n", 0, BOLD, 0);
	modyfiedText(5, 0, "Shoot . . . . . . . . Space\n", 0, BOLD, 0);
	modyfiedText(6, 0, "Pause . . . . . . . . P\n", 0, BOLD, 0);
	// modyfiedText(7, 0, "Select  . . . . . . . Enter\n", 0, BOLD, 0);

}

void Game::displayStars(void)
{
	static Stars	stars[42];
	int i = 0;
	while (i < 42)
	{
		stars[i].displayStar();
		stars[i++].updateCoords();
	}
}

int Game::displayGameOver(Player &player) {
	if (player._life == 0) {
		while (1) {
			modyfiedText((_scr_height / 2) - 1, (_scr_width / 2) - 5, "GAME OVER!", RED, BOLD, BLINK);
			modyfiedText((_scr_height / 2) + 1, (_scr_width / 2) - 7, "Press 'ESC' for exit", 0, BOLD, 0);
			refresh();
			if (keysHandler(player) == KEY_ESC)
				return (0);
		}
	}
	player._life--;
	while (1) {
		clear();
		modyfiedText((_scr_height / 2) - 1, (_scr_width / 2) - 5, "TRY ONE MORE TIME!", RED, BOLD, BLINK);
		modyfiedText((_scr_height / 2), (_scr_width / 2) - 10, "Press 'SPACE' to try again", 0, BOLD, 0);
		modyfiedText((_scr_height / 2) + 1, (_scr_width / 2) - 7, "Or 'ESC' for exit", 0, BOLD, 0);
		refresh();
		if (keysHandler(player) == KEY_SP)
			return (1);
		else if (keysHandler(player) == KEY_ESC)
			return (0);
	}
	return (1);
}

int Game::displayPause(Player &player)
{
	while(1)
	{
		clear();
		modyfiedText((_scr_height / 2) - 1, (_scr_width / 2) - 2, "PAUSE", YELLOW, BOLD, BLINK);
		modyfiedText((_scr_height / 2), (_scr_width / 2) - 10, "Press 'SPACE' to resume", 0, BOLD, 0);
		modyfiedText((_scr_height / 2) + 1, (_scr_width / 2) - 7, "Or 'ESC' for exit", 0, BOLD, 0);
		refresh();
 		if (keysHandler(player) == KEY_SP)
 		{
 			this->_pause = false;
			return(1);
 		}
		else if (keysHandler(player) == KEY_ESC)
			return (0);
	}
	return (1);
}

void Game::displayEnemys(Player &pl1)
{
	static Enemy	enemys[8];

	int i = 0;
	while (i < 8)
	{
		enemys[i].displayEnemy();
		enemys[i].displayBullet();
		enemys[i++].moveEnemy(pl1);
	}
}

void Game::displayBoss(Player &player)
{

	static Boss bastard;
	bastard.drawBoss();
	bastard.moveBoss(player);
//	if (player.getAmount() == 200) {
//		exit(0);
	if (bastard.bullet->_xy.y > _scr_height) {
		bastard.bullet->_xy.y = bastard.getY();
		bastard.bullet->_xy.x = bastard.getX();

	}
	bastard.displayBullet();
	bastard.bullet->_xy.y += 2;
	if (bastard.bullet->checkCollision(player._xy.x, player._xy.y) == 1) {
		this->displayGameOver(player);
	}
//		player.setAmount(0);
//	}
}

int Game::_scr_width = 0;
int Game::_scr_height = 0;
bool Game::_collision = false;
