// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Player.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:39:57 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:40:28 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "../includes/retro.hpp"

Player::Player() : SpaceObject("Player", 0, 0, 50, 1)
{
	this->_life = 5;
	this->_xy.x = Game::_scr_width / 2;
	this->bullet = new Bullet(this->_xy.x, this->_xy.y);
	this->_xy.y = Game::_scr_height - 9;
	this->_ap = 100;
	this->amountOfKilledEnemies = 0;
}

Player::Player(Player const &obj) : SpaceObject::SpaceObject(obj) 
{
	if (this != &obj) {
		*this = obj;
	}
}

Player &Player::operator=(Player const &rhs)
{
	if (this != &rhs) {
		SpaceObject::operator=(rhs);
	}
	return (*this);
}

Player::~Player() {
	return ;
}

void Player::equip(AWeapon *weapon) {
	this->_weapon = weapon;
}

void Player::recover(SpaceObject *obj) {
	(void)obj;
}

int Player::getAP() const { return this->_ap; }
std::string Player::getWeaponType() const {
	if (this->_weapon) {
		return this->_weapon->getName();
	}
	return (0);
}
void			Player::displayPlayer(void)
{
	int blinking = 0;
	static int cnt = 0;
	if (this->_blink)
	{
		cnt++;
		if (cnt < 142)
			blinking = A_BLINK;		
		else 
		{
			cnt = 1;
			this->_blink = false;
		}
	}
	init_pair(3, COLOR_GREEN, COLOR_BLACK);
	attron(A_BOLD);
	if (blinking)
		attron(A_BLINK);
	attron(COLOR_PAIR(3));
	mvprintw(this->_xy .y, this->_xy.x, " .'.");
	mvprintw(this->_xy.y + 1, this->_xy.x, " |o|");
	mvprintw(this->_xy.y + 2, this->_xy.x, ".'o'.");
	mvprintw(this->_xy.y + 3, this->_xy.x, "|.-.|");
	mvprintw(this->_xy.y + 4, this->_xy.x, "'   '");
	mvprintw(this->_xy.y + 5, this->_xy.x, " ( )");
	mvprintw(this->_xy.y + 6, this->_xy.x, "  )");
	mvprintw(this->_xy.y + 7, this->_xy.x, " ( )");
	attron(COLOR_PAIR(3));
	attroff(A_BOLD);
	if (blinking)
		attroff(A_BLINK);
}

void Player::setAmount(int i) {
	this->amountOfKilledEnemies += i;
}

long int Player::getAmount() { return this->amountOfKilledEnemies; }


bool Player::_blink = false;
