// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Weapon.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:42:45 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:42:55 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "retro.hpp"

AWeapon::AWeapon(std::string name, int apcost, int damage)
: _name(name), _apCost(apcost), _damage(damage) {
	return ;
}

AWeapon::AWeapon(AWeapon const &obj)
{
	if (this != &obj) {
		*this = obj;
	}
}

AWeapon &AWeapon::operator=(AWeapon const &rhs)
{
	if (this != &rhs) {
		this->_name = rhs._name;
		this->_apCost = rhs._apCost;
		this->_damage = rhs._damage;
	}
	return (*this);
}

AWeapon::~AWeapon() {
	return;
}

std::string AWeapon::getName() const { return this->_name; }
int AWeapon::getDamage() const { return this->_damage; }
int AWeapon::getAPCost() const { return this->_apCost; }
