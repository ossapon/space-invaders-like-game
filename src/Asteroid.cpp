// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Asteroid.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:45:02 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:45:14 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "retro.hpp"


Asteroid::Asteroid(): SpaceObject("Asteroid", 0, 0, 10, 10)
{

}

Asteroid::Asteroid(Asteroid const &obj) : SpaceObject(obj)
{
}

Asteroid &Asteroid::operator=(Asteroid const &rhs)
{
	(void)rhs;
	return (*this);
}

Asteroid::~Asteroid()
{

}
	