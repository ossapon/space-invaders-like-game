// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 19:39:50 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 19:40:24 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "retro.hpp"
#include <ncurses.h>
#include <unistd.h>
#include <time.h>

int main()
{
	Game	session;
 	Player	pl1;
 	int 	seconds = 0;
 	srand(time(NULL));
	while (true)
	{
		clear();
		if (session.keysHandler(pl1) == KEY_ESC)
			break;
		session.displayStars();
		pl1.displayPlayer();
		session.displayBoss(pl1);
		session.displayEnemys(pl1);
		if (session._show_menu)
			session.displayMenu();
		else
			session.modyfiedText(0, 0, "Press 'm' to show menu.\n", CYAN, BOLD, 0);
		session.modyfiedText(0, (session._scr_width / 2 - 9),"Press ESC to exit.\n", WHITE, BOLD, BLINK);
		if (session._collision)
		{
			if (session.displayGameOver(pl1) == 1)
			{
				Player::_blink = true;
				session._collision = false;
				continue ;
			}
			else
				break;

		}
		if (session._pause)
		{
			if (session.displayPause(pl1) == 1)
				continue ;
			else
				break;

		}
		if (session._bullet) {
			pl1.bullet->displayBullet();
			pl1.bullet->move();
			pl1.setAmount(100);
		}
		refresh(); 
		usleep(50000);
		seconds++;
	}
	return (0);
}
