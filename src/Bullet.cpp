
#include "../includes/retro.hpp"
#include <cmath>

Bullet::Bullet(int x, int y) : SpaceObject("Bullet", x, y, 1, 1) {
	return ;
}

Bullet::Bullet(Bullet const &obj) : SpaceObject(obj) {
	return ;
}

Bullet::~Bullet() {
	delete this;
}

Bullet& Bullet::operator=(Bullet const &rhs) {
	if (this != &rhs) {
		SpaceObject::operator=(rhs);
	}
	return *this;
}

void Bullet::displayBullet() {
	init_pair(5, COLOR_YELLOW, COLOR_BLUE);
	attron(A_BOLD);
	attron(COLOR_PAIR(5));
	mvprintw(this->_xy.y, this->_xy.x, "*");
	attroff(A_BOLD);
	attroff(COLOR_PAIR(5));
}

void Bullet::move() {
	this->_xy.y--;
	if (this->_xy.y == 0 || this->_collision) {
		;
	}
}

int	Bullet::checkCollision(int x, int y) {
	if ((fabs(x - this->_xy.x)  <= 1.0) && (fabs(y - this->_xy.y) <= 1.0))
	{
		return 1;
	}
	return (0);

}
