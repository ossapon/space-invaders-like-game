// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Wall.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:45:35 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:45:48 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "../includes/retro.hpp"

Wall::Wall() : SpaceObject("Wall", 0, 0, 0, 0)
{

}

Wall::Wall(Wall const &obj) : SpaceObject(obj)
{
	(void)obj;

}

Wall &Wall::operator=(Wall const &rhs)
{
	(void)rhs;
	return (*this);
}

Wall::~Wall()
{

}
