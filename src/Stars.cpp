// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Stars.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:45:35 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:45:48 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "retro.hpp"

Stars::Stars(void)
{
	this->_x = rand() % Game::_scr_width;
	this->_y = rand() % Game::_scr_height;
}

void Stars::updateCoords(void)
{
	this->_y++;
	if (this->_y >= Game::_scr_height)
		this->_y = 0;
}

void Stars::displayStar(void)
{
	static int cnt = 0;
	const char	*star;
	int		color;
	color = cnt % 12 ? 7 : rand() % 6;
	if (cnt % 12)
		star = ".";
	else if (cnt % 5)
		star = "*";
	else 
		star = "*";
	Game::modyfiedText(this->_y, this->_x, star, color , 0, 0);
	cnt++;
}

Stars::Stars(Stars const &obj)
{
	(void)obj;

}

Stars &Stars::operator=(Stars const &rhs)
{
	(void)rhs;
	return (*this);
}

Stars::~Stars()
{

}
