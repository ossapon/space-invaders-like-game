
#include "retro.hpp"

Pistol::Pistol() : AWeapon("Pistol", 1, 10) {
	return ;
}

Pistol::Pistol(Pistol const &rhs) : AWeapon(rhs) {
	return ;
}

Pistol& Pistol::operator=(Pistol const &rhs) {
	if (this != &rhs) {
		AWeapon::operator=(rhs);
	}
	return *this;
}

int Pistol::attack() const {
	return this->getDamage();
}
