// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Boss.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:41:10 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:41:21 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "../includes/Boss.hpp"
#include "Game.hpp"
#include "Player.hpp"
#include <string>
#include <cmath>

Boss::Boss(void)
{
	this->_x = Game::_scr_width / 2 - 21;
	this->_y = Game::_scr_height * 0.15;
	this->bullet = new Bullet(this->_x, this->_y);
//	this->displayBullet = 0;
}


Boss::Boss(std::string type, int hp) : _hp(hp), _type(type) {
	return;
}

Boss::Boss(Boss const &obj)
{
	if (this != &obj) {
		*this = obj;
	}
}

Boss &Boss::operator=(Boss const &rhs)
{
	if (this != &rhs) {
		this->_hp = rhs._hp;
		this->_type = rhs._type;
	}
	return *this;
}

Boss::~Boss() {
	return;
}

void Boss::takeDamage(int damage) {
	if (this->_hp > 0)
		this->_hp = (this->_hp > damage) ? this->_hp - damage : 0;
}

int Boss::getHP() const { return this->_hp; }
std::string Boss::getType() const { return this->_type; }

void			Boss::drawBoss(void)
{
	init_pair(4, CYAN, COLOR_BLACK);
	attron(COLOR_PAIR(4));
	mvprintw(this->_y - 7, this->_x, "                   _|_");
	mvprintw(this->_y - 6, this->_x, "                  / _ \\");
	mvprintw(this->_y - 5, this->_x, "               __/ (_) \\__");
	mvprintw(this->_y - 4, this->_x, "          ____/_ ======= _\\____");
	mvprintw(this->_y - 3, this->_x, " ________/ _ \\(_)_______(_)/ _ \\_______");
	mvprintw(this->_y - 2, this->_x, "<___+____ (_) | /  _  \\ | (_) ____+___>");
	mvprintw(this->_y - 1, this->_x, " O O O  \\___/ |   (_)   | \\___/  O O O");
	mvprintw(this->_y, this->_x, "            \\__\\_______/__/");
	attroff(COLOR_PAIR(4));
}

void			Boss::moveBoss(Player &player)
{
	int start_val = this->_x;
	if (player._xy.x != this->_x + 21)
		this->_x += player._xy.x > this->_x ? 0.5 : -0.5;
	if(this->_x == player._xy.x || this->_x + 42 >= Game::_scr_width ||
	   this->_x - 1 <= 0)
	{
		this->_x = start_val;
		return;
	}
	else if (this->_x + 41 > Game::_scr_width)
		this->_x--;
	else if (this->_x - 1 < 0)
		this->_x++;
	if ((fabs(player._xy.x - this->_x)  >= 0.5 &&(fabs(player._xy.x - this->_x)  <= 42.0)) && (fabs(player._xy.y - this->_y) <= 1.2))
	{
		if(!(Player::_blink))
			Game::_collision = true;
	}

}

void Boss::displayBullet() {
	init_pair(7, COLOR_YELLOW, COLOR_RED);
	attron(A_BOLD);
	attron(COLOR_PAIR(7));
	mvprintw(this->bullet->_xy.y, this->bullet->_xy.x + 2, "*");
	mvprintw(this->bullet->_xy.y + 1, this->bullet->_xy.x, "*-*-*");
	mvprintw(this->bullet->_xy.y + 2, this->bullet->_xy.x, "*-*-*");
	mvprintw(this->bullet->_xy.y + 3, this->bullet->_xy.x + 2, "*");
	mvprintw(this->bullet->_xy.y, this->bullet->_xy.x + 2 + 38, "*");
	mvprintw(this->bullet->_xy.y + 1, this->bullet->_xy.x + 38, "*-*-*");
	mvprintw(this->bullet->_xy.y + 2, this->bullet->_xy.x + 38, "*-*-*");
	mvprintw(this->bullet->_xy.y + 3, this->bullet->_xy.x + 2 + 38, "*");
	attroff(A_BOLD);
	attroff(COLOR_PAIR(7));
}

double Boss::getY() const { return this->_y; }
double Boss::getX() const { return this->_x; }