// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Enemy.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:41:10 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:41:21 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "../includes/retro.hpp"
#include <cmath>

Enemy::Enemy() {
	this->_y = (rand() % 8) - 10;
		this->_x = rand() % (Game::_scr_width);
		this->collision = 0;
	this->bullet = new Bullet(this->_x, this->_y);
}

Enemy::Enemy(std::string type, int hp) : _hp(hp), _type(type) {
	this->_x = rand() % (Game::_scr_width);
	this->_y = (rand() % 25) - 10;
	return;
}

Enemy::Enemy(Enemy const &obj)
{
	if (this != &obj) {
		*this = obj;
	}
}

Enemy &Enemy::operator=(Enemy const &rhs)
{
	if (this != &rhs) {
		this->_hp = rhs._hp;
		this->_type = rhs._type;
	}
	return *this;
}

Enemy::~Enemy() {
	return;
}

void Enemy::takeDamage(int damage) {
	if (this->_hp > 0)
		this->_hp = (this->_hp > damage) ? this->_hp - damage : 0;
}

int Enemy::getHP() const { return this->_hp; }
std::string Enemy::getType() const { return this->_type; }

void			Enemy::displayEnemy(void)
{
	if (this->collision != 1) {
		init_pair(2, COLOR_RED, COLOR_BLACK);
		attron(COLOR_PAIR(2));
		mvprintw(this->_y - 3, this->_x, "  |  ");
		mvprintw(this->_y - 2, this->_x, "_'o'_");
		mvprintw(this->_y - 1, this->_x, "|.-.|");
		mvprintw(this->_y, this->_x, " +|+  ");
		attroff(COLOR_PAIR(2));
	}
}

void			Enemy::displayBullet(void)
{
	init_pair(6, COLOR_BLACK, COLOR_WHITE);
	attron(A_BOLD);
	attron(COLOR_PAIR(6));
	mvprintw(this->bullet->_xy.y, this->bullet->_xy.x, "*");
	attroff(A_BOLD);
	attroff(COLOR_PAIR(6));
	this->bullet->_xy.y += 1;
}

void			Enemy::moveEnemy(Player &pl1)
{
	static int temp = 0;

	if (this->_y > Game::_scr_height)
	{
		this->_y = (rand() % 8) - 10;
		this->_x = rand() % (Game::_scr_width);
		this->collision = 0;
		this->bullet->_xy.y = this->_y;
		this->bullet->_xy.x = this->_x;
	}	
	else {
		this->_y += 0.2;

		if (temp == Game::_scr_height) {
			this->bullet->_xy.y = this->_y;
			this->bullet->_xy.x = this->_x;
			temp = 0;
		}
		temp++;
	}
	if (((fabs(pl1._xy.x - this->_x)  <= 1.0) && (fabs(pl1._xy.y - this->_y) <= 1.0))
	|| ((fabs(pl1._xy.x - this->bullet->_xy.x)  <= 1.0) && (fabs(pl1._xy.y - this->bullet->_xy.y) <= 1.0)))
	{
		if (this->collision == 0)
			if(!(Player::_blink))
				Game::_collision = true;
	}
	if (((fabs(pl1.bullet->_xy.x - this->_x)  <= 2.0) && (fabs(pl1.bullet->_xy.y - this->_y) <= 1.0))
		|| ((fabs(pl1._xy.x - this->bullet->_xy.x)  <= 1.0) && (fabs(pl1._xy.y - this->bullet->_xy.y) <= 1.0))) {

		this->collision = 1;
	}
}
