// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   SpaceObject.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/06 21:44:11 by soleksiu          #+#    #+#             //
//   Updated: 2018/10/06 21:44:27 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "retro.hpp"

SpaceObject::SpaceObject(std::string type, int x, int y, int val, int speed) : _type(type), _val(val), _speed(speed) {
	this->_collision = 0;
	this->_life = 1;
	this->_lvl = 1;
	this->_xy.x = x;
	this->_xy.y = y;
}

SpaceObject::SpaceObject(SpaceObject const &obj)
{
	if (this != &obj) {
		*this = obj;
	}
}

SpaceObject &SpaceObject::operator=(SpaceObject const &rhs)
{
	if (this != &rhs) {
		this->_xy.x = rhs._xy.x;
		this->_xy.y = rhs._xy.y;
		this->_speed = rhs._speed;
		this->_type = rhs._type;
		this->_val = rhs._val;
		this->_life = rhs._life;
		this->_lvl = rhs._lvl;
	}
	return (*this);
}

SpaceObject::~SpaceObject() {

}

int SpaceObject::getSpeed() const { return this->_speed; }
std::string SpaceObject::getType() const { return this->_type; }
int SpaceObject::getVal() const { return this->_val; }
int SpaceObject::getX() const { return this->_xy.x; }
int SpaceObject::getY() const { return this->_xy.y; }
void SpaceObject::setX(int x) { this->_xy.x = x; }
void SpaceObject::setY(int y) { this->_xy.y = y; }
